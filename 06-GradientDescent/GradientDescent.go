package main

import (
	"fmt"
	"math"
)

type function func(float64) float64

func optimize(fn function, derFn function, x0 float64, alpha float64, err float64) float64 {
	xNew := x0
	var xOld float64
	diffNewOld := err + 1

	for diffNewOld > err {
		xOld = xNew
		xNew = xOld - alpha*derFn(xOld)
		// xNew -= alpha*derFn(xOld)
		diffNewOld = math.Abs(xNew - xOld)
	}
	return xNew
}

func fn1(x float64) float64 {
	return x * (x - 10) // x*x - 10*x
}
func derFn1(x float64) float64 {
	return 2*x - 10
}

func main() {
	xOpt := optimize(fn1, derFn1, -1230, 0.01, 1e-30)
	fmt.Println("Optimum:", xOpt)
}
