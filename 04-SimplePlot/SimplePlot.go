package main

import (
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/plotutil"
	"gonum.org/v1/plot/vg"
)

func main() {

	p, err := plot.New()

	p.Title.Text = "Simple plot"
	p.X.Label.Text = "X"
	p.Y.Label.Text = "Y"

	points := make(plotter.XYs, 30)
	for i := range points {
		points[i].X = float64(i)
		points[i].Y = float64(i) * float64(i)
	}

	err = plotutil.AddLinePoints(p, "Quadratic", points)
	if err != nil {
		panic(err)
	}

	if err := p.Save(4*vg.Inch, 4*vg.Inch, "SimplePlot.png"); err != nil {
		panic(err)
	}

}
