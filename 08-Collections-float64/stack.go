package main

import (
	"fmt"
)

// Stack is representation of LIFO data structure
type Stack struct {
	head *Item
}

// Push pushes (insert) a value to the stack
func (stack *Stack) Push(value float64) {
	stack.head = &Item{value: value, next: stack.head}
}

// Pop pops value (remove and return) value from the stack
func (stack *Stack) Pop() float64 {
	head := stack.head
	stack.head = head.next
	return head.value
}

// NewStack initializes Stack structure from slice
func NewStack(vector []float64) Stack {
	myStack := Stack{}
	for i := len(vector) - 1; i >= 0; i-- {
		myStack.Push(vector[i])
	}
	return myStack
}

// Len returns length of the stack
func (stack Stack) Len() int {
	item := stack.head
	n := 1
	for item.next != nil {
		item = item.next
		n++
	}
	return n
}

// ToString converts Stack structure to string.
func (stack Stack) ToString() string {
	item := stack.head
	str := "["
	for true {
		str += fmt.Sprint(item.value)
		item = item.next
		if item == nil {
			str += "]"
			break
		} else {
			str += ", "
		}
	}
	return str
}
