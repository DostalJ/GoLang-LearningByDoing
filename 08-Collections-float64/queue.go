package main

import (
	"fmt"
)

// Queue is representation of FIFO data structure
type Queue struct {
	head *Item
	tail *Item
}

func (queue *Queue) Enque(val float64) {
	newItem := &Item{value: val, next: queue.tail, previous: nil}
	fmt.Println(newItem.value)
	queue.tail.previous = newItem
	// queue.tail = newItem
}

func NewQueue(vec []float64) Queue {
	myQueue := Queue{}
	for i := len(vec) - 1; i >= 0; i-- {
		myQueue.Enque(vec[i])
	}
	return myQueue
}

// func (queue *Queue) Deque() float64 {
// 	val := queue.tail.value
// 	queue.tail = nil
// 	return val
// }

func (queue Queue) ToString() string {
	item := queue.head
	str := "["
	for true {
		str += fmt.Sprint(item.value)
		item = item.next
		if item == nil {
			str += "]"
			break
		} else {
			str += ", "
		}
	}
	return str
}
