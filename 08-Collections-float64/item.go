package main

// Item object containing value and pointer to next Item
type Item struct {
	value    float64
	next     *Item
	previous *Item
}
