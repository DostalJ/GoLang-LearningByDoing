package main

import (
	"fmt"
	"math"
	"math/rand"
)

func isEqual(vec1, vec2 []float64) bool {
	if len(vec1) != len(vec2) {
		return false
	}
	for i := range vec1 {
		if vec1[i] != vec2[i] {
			return false
		}
	}
	return true
}
func isEqual2D(mat1, mat2 [][]float64) bool {
	if len(mat1) != len(mat2) || len(mat1[0]) != len(mat2[0]) {
		return false
	}
	for i := range mat1 {
		if !isEqual(mat1[i], mat2[i]) {
			return false
		}
	}
	return true
}

// true if ofe of rows of 'matrix' is 'vec'
func isInMatrix(vec []float64, matrix [][]float64) bool {
	for _, row := range matrix {
		if isEqual(vec, row) {
			return true
		}
	}
	return false
}

// Add to vectors
func add(vec1 []float64, vec2 []float64) []float64 {
	var vec3 []float64
	for i := 0; i < len(vec1); i++ {
		vec3 = append(vec3, vec1[i]+vec2[i])
	}
	return vec3
}

// Multiply a vector by a scalar
func multiply(vec1 []float64, alpha float64) []float64 {
	var vec3 []float64
	for i := 0; i < len(vec1); i++ {
		vec3 = append(vec3, alpha*vec1[i])
	}
	return vec3
}

// Dot product of two vectors
func dotProduct(vec1, vec2 []float64) float64 {
	var res float64
	res = 0
	for i := 0; i < len(vec1); i++ {
		res += vec1[i] * vec2[i]
	}
	return res
}

// Distance between two vectors
// dist(x, y) = ||x-y|| = sqrt((x-y)*(x-y)), where '*' is dot product
func dist(x, y []float64) float64 {
	vecDiff := add(x, multiply(y, -1.0)) // x - y
	d := math.Sqrt(dotProduct(vecDiff, vecDiff))
	return d
}

type KMeans struct {
	k         int
	centroids [][]float64
	classes   []int
}

// Randomly pics 'kMeans.k' rows from data and sets them as centroids
func (kMeans *KMeans) initializeCentroids(data [][]float64) {
	// modifying <-- pointer (without pointer it only receives a copy)
	var centroids [][]float64
	for len(centroids) < kMeans.k {
		randCentr := data[rand.Intn(len(data))]
		if !isInMatrix(randCentr, centroids) {
			centroids = append(centroids, randCentr)
		}
	}
	fmt.Println("Initialized centroids:", centroids)
	kMeans.centroids = centroids
}

func findClass(point []float64, centroids [][]float64) int {
	class := 0
	minDist := dist(point, centroids[0])
	for i, centroid := range centroids {
		d := dist(point, centroid)
		if d < minDist {
			class = i
			minDist = d
		}
	}
	return class
}

func updateClasses(data, centroids [][]float64) []int {
	var classes []int
	for _, point := range data {
		cls := findClass(point, centroids)
		classes = append(classes, cls)
	}
	return classes
}

func updateCentroids(data [][]float64, classes []int) [][]float64 {
	var numSamplesInClass []int
	for _, val := range classes {
		if len(numSamplesInClass) < val+1 {
			for i := len(numSamplesInClass) - 1; i <= val; i++ {
				numSamplesInClass = append(numSamplesInClass, 0)
			}
		}
		numSamplesInClass[val] += 1
	}
	numClasses := len(numSamplesInClass)
	numFeatures := len(data[0])

	// initialize 'centroids' with zeros
	centroids := make([][]float64, numClasses)
	for i := range centroids {
		centroids[i] = make([]float64, numFeatures)
	}
	// compute new centroids
	for i, vec := range data {
		cls := classes[i]
		centroids[cls] = add(centroids[cls], multiply(vec, 1/float64(numSamplesInClass[cls])))
	}
	// fmt.Println(centroids)
	return centroids
}

func (kMeans *KMeans) setCentroids(centroids [][]float64) {
	kMeans.centroids = centroids
}

// Fit data and save classes data[samples, features]
func (kMeans *KMeans) fit(data [][]float64) {
	// initialize centroids
	kMeans.initializeCentroids(data)

	classes := updateClasses(data, kMeans.centroids)
	centroids := updateCentroids(data, classes)

	for !isEqual2D(centroids, kMeans.centroids) {
		kMeans.centroids = centroids
		classes = updateClasses(data, kMeans.centroids)
		centroids = updateCentroids(data, classes)
	}
	kMeans.centroids = centroids
	kMeans.classes = classes
}

// Fit data, save and return classes data[samples, features]
func (kMeans *KMeans) fitTransform(data [][]float64) []int {
	kMeans.fit(data)
	return kMeans.classes
}

func main() {

	data := [][]float64{
		{1, 1, 3},
		{2, 2, 1},
		{0, 1, 2},
		{9, 5, 7},
		{11, 12, 4},
	}

	kMeans := KMeans{k: 2}
	classes := kMeans.fitTransform(data)
	fmt.Print(classes)

}
