package main

import (
	"fmt"
)

func add(vec1 []float64, vec2 []float64) []float64 {
	var vec3 []float64
	for i := 0; i < len(vec1); i++ {
		vec3 = append(vec3, vec1[i]+vec2[i])
	}
	return vec3
}

func multiply(vec1 []float64, alpha float64) []float64 {
	var vec3 []float64
	for i := 0; i < len(vec1); i++ {
		vec3 = append(vec3, alpha*vec1[i])
	}
	return vec3
}

func dotProduct(vec1 []float64, vec2 []float64) float64 {
	var res float64
	res = 0
	for i := 0; i < len(vec1); i++ {
		res += vec1[i] * vec2[i]
	}
	return res
}

func main() {
	vector1 := []float64{1, 2, 3}
	vector2 := []float64{4, 5, 6}
	alpha := 0.5

	vecSum := add(vector1, vector2)
	vecMult := multiply(vector1, alpha)
	vecDotProd := dotProduct(vector1, vector2)

	fmt.Println("vector1:", vector1)
	fmt.Println("vector2:", vector2)
	fmt.Println("alpha:", alpha)
	fmt.Println()
	fmt.Println("Sum:", vecSum)
	fmt.Println("Scalar multiplication:", vecMult)
	fmt.Println("Dot product:", vecDotProd)
}
