package main

import (
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/plotutil"
	"gonum.org/v1/plot/vg"
)

func makePoints(start float64, end float64, num int) []float64 {
	if start > end {
		panic("'start' must be lower than 'end'")
	}
	dx := (end - start) / float64(num)
	var X []float64
	for num := start; num <= end; num += dx {
		X = append(X, num)
	}
	return X
}

type function func(float64) float64

func function1(x float64) float64 {
	return x * x
}

func functionInPoints(fn function, points []float64) []float64 {
	var funvVals []float64
	for _, pt := range points {
		funvVals = append(funvVals, fn(pt))
	}
	return funvVals
}

func deriveInPoints(fn function, points []float64) []float64 {
	dx := 1e-4
	var derivedVals []float64
	for _, pt := range points {
		pointDerivation := (fn(pt+dx) - fn(pt)) / dx
		derivedVals = append(derivedVals, pointDerivation)
	}
	return derivedVals
}

func plotXY(p *plot.Plot, X, Y []float64, name string) *plot.Plot {
	if len(X) != len(Y) {
		panic("'X' and 'Y' should have a same length")
	}

	points := make(plotter.XYs, len(X))
	for i := range points {
		points[i].X = X[i]
		points[i].Y = Y[i]
	}

	err := plotutil.AddLinePoints(p, name, points)
	if err != nil {
		panic(err)
	}
	return p
}

func main() {
	X := makePoints(-15, 15, 100)
	Y := functionInPoints(function1, X)
	derY := deriveInPoints(function1, X)

	p, err := plot.New()
	if err != nil {
		panic(err)
	}
	plotXY(p, X, Y, "Function")
	plotXY(p, X, derY, "Derivation")
	if err := p.Save(4*vg.Inch, 4*vg.Inch, "Derivation.png"); err != nil {
		panic(err)
	}
}
