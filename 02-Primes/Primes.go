package main

import (
	"fmt"
	"math"
)

func isPrime(n int) bool {
	for i := 2; float64(i) < math.Floor(math.Sqrt(float64(n))); i++ {
		if n%i == 0 {
			return false
		}
	}
	return true
}

func erasthotenes(n int) []int {
	nums := make([]int, n)
	for i := 1; i <= n; i++ {
		nums[i-1] = i
	}
	for i := 2; float64(i) <= math.Floor(math.Sqrt(float64(n))); i++ {
		for j := i; j < n; j++ {
			if nums[j]%i == 0 {
				nums[j] = 0
			}
		}
	}
	var onlyPrimes []int
	for _, val := range nums {
		if val != 0 {
			onlyPrimes = append(onlyPrimes, val)
		}
	}
	return onlyPrimes
}

func main() {
	fmt.Println(erasthotenes(20))
}
